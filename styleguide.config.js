const path = require('path');
const webpackDevConfig = require('./config/webpack.config.dev.js');

module.exports = {
    webpackConfig: webpackDevConfig,
    require: [
        path.join(__dirname, 'public/jquery.min.js'),
        path.join(__dirname, 'public/material_icons.css'),
        path.join(__dirname, 'public/materialize.min.css'),
        path.join(__dirname, 'public/materialize.min.js'),
        path.join(__dirname, 'public/icons.ttf'),
        path.join(__dirname, 'public/icons.woff2'),
    ]
}