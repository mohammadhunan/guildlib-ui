examples:

```js
const materialize = require('materialize-css');
<ResizableBarMaterializeCss rightItems={[
  {
      path: '/',
      icon: 'help',
      label: 'help'
  },
  {
      path: '/',
      icon: 'apps',
      label: 'apps'
  },
  {
      path: '/',
      icon: 'person_outline',
      label: 'your account'
  }
]} isGuildNavBar/>
```