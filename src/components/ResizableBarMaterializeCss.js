import React from 'react';
import PropTypes from 'prop-types';
import './ResizableBarMaterializeCss.css';
import Logo from '../assets/logo.svg';
require('materialize-css');
/* 
    These components should not 
    be connected to state, and 
    should rely completely on 
    passing props to it. If they 
    need to interact with state 
    in  some compacitity, a seperate
    component should be created which
    renders this one (as to not effect)
    multiple projects by changing a 
    reusable component. Alternatively,
    methods to interact with state can be
    passed in as props via the parent of
    this component.

*/ 

/** @component */
let ResizableBarMaterializeCss = ({
    isGuildNavBar,
    rightItems,
    barStyles,
}) => (
    <div className="resizable-bar-materialize-css">
        <nav>
            <div className="nav-wrapper guild-ui-nav" style={barStyles}>
            {
                isGuildNavBar?
                (
                <a 
                    href="/"
                    className="brand-logo">
                    <i className="material-icons guild-ui-icon">menu</i>
                    <img 
                        src={Logo}
                        alt="Logo"
                        className="standard-logo"
                    />
                </a>
                ): null
            }
            <ul 
                id="nav-mobile"
                className="right hide-on-med-and-down"
            >
                {
                    rightItems.map((item,i) => {
                        return(<li key={"li" + i}>
                            <a href={item.path}>
                                {
                                    item.icon? <i className="material-icons guild-ui-icon">{item.icon}</i> : item.label
                                }
                            </a>
                        </li>)
                    })
                }
            </ul>
            </div>
        </nav>
    </div>
)



ResizableBarMaterializeCss.propTypes = {
    isGuildNavBar: PropTypes.bool,
    rightItems: PropTypes.array,
    barStyles: PropTypes.object,
}


ResizableBarMaterializeCss.defaultProps = {
    isGuildNavBar: false,
    rightItems: [],
    barStyles: {},
}

export default ResizableBarMaterializeCss;