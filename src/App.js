import React, { Component } from 'react';
import {Button, Icon} from 'react-materialize';
import ResizableBarMaterializeCss from './components/ResizableBarMaterializeCss';
import './App.css';
const navBarRightItems = [
  {
      path: '/',
      icon: 'help',
      label: 'help'
  },
  {
      path: '/',
      icon: 'apps',
      label: 'apps'
  },
  {
      path: '/',
      icon: 'person_outline',
      label: 'your account'
  }
]

class App extends Component {
  
  render() {
    return (
      <div className="App">
          <ResizableBarMaterializeCss rightItems={navBarRightItems} isGuildNavBar/>
      </div>
    );
  }
}

export default App;
